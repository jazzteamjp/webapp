window.onload = function () {

    var self = this;

    self.baseUrl = "http://localhost:8765/robotics";

    self.jobs = ko.observableArray([]);
    self.tasks = ko.observableArray([]);
    self.robotTypes = ko.observableArray([]);
    self.selectedType = ko.observable();
    self.taskName = ko.observable();
    self.duration = ko.observable();
    self.isKilling = ko.observable();
    self.lastJobId = ko.observable();

    function ViewModel() {
        var selfVm = this;
        this.jobs = self.jobs;
        this.tasks = self.tasks;
        this.robotTypes = self.robotTypes;
        this.taskName = self.taskName;
        this.duration = self.duration;
        this.isKilling = self.isKilling;
        this.selectedType = self.selectedType;
        this.lastJobId = self.lastJobId;

        selfVm.sendSingleTask = function(task) {
            $.ajax(self.baseUrl + "/tasks/" + task.id + "?broadcast=false", {
                contentType : 'application/json',
                type : 'POST',
                success: function () {
                    self.getTasks();
                }
            });
        };

        selfVm.sendBroadcastTask = function(task) {
            $.ajax(self.baseUrl + "/tasks/" + task.id + "?broadcast=true", {
                contentType : 'application/json',
                type : 'POST',
                success: function () {
                    self.getTasks();
                }
            });
        };

        selfVm.addSkillToRobot = function(task) {
            $.ajax(self.baseUrl + "/tasks/" + task.id + "/types/" + self.selectedType(), {
                contentType : 'application/json',
                type : 'POST',
                success: function () {
                    self.getTasks();
                }
            });
        };

        selfVm.addTask = function() {
            var newTask = {
                name: self.taskName(),
                duration: self.duration(),
                isKilling: self.isKilling()
            };
            $.ajax(self.baseUrl + "/tasks", {
                contentType : 'application/json',
                type : 'POST',
                data : JSON.stringify(newTask),
                success: function () {
                    self.getTasks();
                }
            });
        };
    }

    ko.applyBindings(new ViewModel());

    self.getJobs = function () {
        $.ajax(self.baseUrl + "/tasks/logs", {
            type : 'GET',
            success: function(jobs) {
                // $(function () {
                //     $('[data-toggle="tooltip"]').tooltip('hide')
                // });
                self.jobs(jobs);

                // $(function () {
                //     $('[data-toggle="tooltip"]').tooltip()
                // });
                setTimeout(self.getJobs, 3000);
            }
        });
    };

    self.getTasks = function () {
        $.ajax(self.baseUrl + "/tasks", {
            type : 'GET',
            success: function(tasks) {
                self.tasks(tasks);
            }
        });
    };

    self.getRobotTypes = function () {
        $.ajax(self.baseUrl + "/tasks/types", {
            type : 'GET',
            success: function(robotTypes) {
                self.robotTypes(robotTypes);
            }
        });
    };

    self.getJobs();
    self.getTasks();
    self.getRobotTypes();
};